import dollarIcon from "../src/assets/image/icon-dollar.svg"
import peopleIcon from "../src/assets/image/icon-person.svg"
import logo from "../src/assets/image/logo.svg"

import { useState } from "react";


 

export default function App() {
  const [bill, setBill] = useState(0);
  const [percent, setPercent] = useState(null);
  const [people, setPeople] = useState(0);
  

  const amount = (bill * (percent/100)) / people;
  const total = (bill / people) + amount;

  
  
 

  

  function handleReset() { 
    setBill(0)
    setPercent(0)
    setPeople(0)
  }
  return (
    <div className="wrapper">
      <img src={logo} alt="Splitter-Logo" />
      <div className="container">
        <div>
          <ValueForm
            bill={bill}
            setBill={setBill}
            percent={percent}
            people={people}
            setPeople={setPeople} 
            setPercent={setPercent} 
          />
        </div>
        <div className="display">
          <Calculator amount={amount} total={total} onReset={handleReset}/>
        </div>
      </div>
    </div>
  );
}

function Calculator({amount,total,onReset}) {
  return (
    <div className="display">
      <div className="display-row">
          <div className="display-label">
            <p className="header">Tip Amount</p>
            <p className="unit">/ person</p>
        </div>
            <p className="display-amt">$ {amount ? amount: "0.00"}</p>
      </div>
      <div className="display-row">
          <div className="display-label">
            <p className="header">Total</p>
            <p className="unit">/ person</p>
          </div>
        <p className="display-amt">$ {total ? total : "0.00"}</p>
      </div>{
        total ? (<button class="btn" onClick={onReset}>Reset</button>) : (<button class="btn" onClick={onReset} disabled>Reset</button>)
      }
      
  </div>
  )
}

function BillForm({bill, setBill}) { 
  
  return (
    <div className="label-group">
      <label className="label">Bill</label>
      <div className="number-wrapper">
      <input type="text" id="bill" placeholder="bill" value={bill} onChange={(e) => setBill(Number(e.target.value))} />
      <img src={dollarIcon} aria-hidden="true" className="icon" alt="dollar"/>
      </div>
    </div>
  )
}

function PeopleForm({people,setPeople}) { 
  
  return (
    <div className="label-group">
      <div className="label-wrapper">
        <label className="label">Number of People</label>
      </div>
      <div className="number-wrapper">
        <input type="text" id="people" placeholder="people" value={people} onChange={(e) => setPeople(Number(e.target.value))} />
        <img src={peopleIcon} aria-hidden="true" className="icon" alt="people" />
      </div>
    </div>
  )
}

function PercentChoice({ percent, setPercent }) {
  
  const handleSelectedPercent = (e) => setPercent(Number(e.target.value))

  return (
    <div className="percent-section">
      <label className="label">Select Tip %</label>
      <div className="percent-amount-wrapper">
      <div className="percent-amount">
        <input type="radio" value="5" name="percent" onChange={handleSelectedPercent}/>
        <div className="percent-btn">5%</div>
      </div>
      <div className="percent-amount">
        <input type="radio" value="10" name="percent" onChange={handleSelectedPercent} />
        <div className="percent-btn">10%</div>
      </div>
      <div className="percent-amount">
        <input type="radio" value="15" name="percent" onChange={handleSelectedPercent} />
        <div className="percent-btn">15%</div>
      </div>
      <div className="percent-amount">
        <input type="radio" value="25" name="percent" onChange={handleSelectedPercent} />
        <div className="percent-btn">25%</div>
      </div>
      <div className="percent-amount">
        <input type="radio" value="50" name="percent" onChange={handleSelectedPercent} />
        <div className="percent-btn">50%</div>
      </div>
      <input type="text" placeholder="custom" onChange={handleSelectedPercent}/>
      </div>
    </div>
  )
}


function ValueForm({bill,setBill,percent,setPercent,people,setPeople}) {
  return (
    <div className="form">
      <BillForm bill={bill} setBill={setBill} />
      <PercentChoice percent={percent} setPercent={setPercent} />
      <PeopleForm people={people} setPeople={setPeople}/>
    </div>
  )
}